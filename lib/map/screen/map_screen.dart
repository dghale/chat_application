import 'package:chats/map/entity/map_module.dart';
import 'package:chats/map/request/map_request.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';

class MapScreen extends StatefulWidget {
  const MapScreen({Key? key, required this.userId}) : super(key: key);

  final String userId;

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  MapRequest mapRequest = MapRequest();

  late GoogleMapController mapController;

  double latitude = 0;
  double longitude = 0;
  LatLng? _center;

  var location = '';
  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      mapController = controller;
    });
  }

  @override
  void initState() {
    checkLocationPermission();
    super.initState();
  }

  getCurrentLocation() async {
    var position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    mapRequest.setMapData(widget.userId,
        MapModule(longitude: position.longitude.toString(), latitude: position.latitude.toString()));


    await mapRequest.getMapData(widget.userId);

    setState(() {
      location = "$position.latitude, $position.longitude";
      _center = LatLng(double.parse(mapRequest.mapModule!.latitude) , double.parse(mapRequest.mapModule!.longitude));
    });

    mapController.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: _center!, zoom: 17)));
  }

  @override
  Widget build(BuildContext context) {
    _center = const LatLng(27.6602292, 85.308027);

    return Scaffold(
        appBar: AppBar(
          title: const Text("Maps"),
        ),
        body: GoogleMap(
            myLocationEnabled: true,
            compassEnabled: true,
            mapToolbarEnabled: true,
            onMapCreated: _onMapCreated,
            initialCameraPosition:
                CameraPosition(target: _center!, zoom: 17.0)));
  }

  // location permission
  checkLocationPermission() async {
    var locationPermission = await Permission.location.request();

    if (locationPermission.isGranted) {
      return getCurrentLocation();
    }
    if (locationPermission.isDenied || locationPermission.isPermanentlyDenied) {
      await openAppSettings();
    }
  }
}
