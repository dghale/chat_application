// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_module.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MapModule _$MapModuleFromJson(Map<String, dynamic> json) => MapModule(
      longitude: json['longitude'] as String? ?? '',
      latitude: json['latitude'] as String? ?? '',
    );

Map<String, dynamic> _$MapModuleToJson(MapModule instance) => <String, dynamic>{
      'longitude': instance.longitude,
      'latitude': instance.latitude,
    };
