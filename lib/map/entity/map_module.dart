import 'package:json_annotation/json_annotation.dart';

part 'map_module.g.dart';

@JsonSerializable()
class MapModule{
  @JsonKey(name: "longitude", defaultValue: "", includeIfNull: true)
  final String longitude;

  @JsonKey(name: "latitude", defaultValue: "", includeIfNull: true)
  final String latitude;

  MapModule({required this.longitude, required this.latitude});

  factory MapModule.fromJson(Map<String, dynamic> map) => _$MapModuleFromJson(map);

  Map<String, dynamic> toJson() => _$MapModuleToJson(this);
}