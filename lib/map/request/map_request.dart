import 'dart:convert';

import 'package:chats/map/entity/map_module.dart';
import 'package:firebase_database/firebase_database.dart';

class MapRequest {
  final databaseRef = FirebaseDatabase.instance.reference();
  MapModule? mapModule;

  setMapData(String userId, MapModule mapModule) {
    databaseRef.child('Users').child(userId).child('Map').set(
        {"longitude": mapModule.longitude, "latitude": mapModule.latitude});
  }

  Future getMapData(String userId) async {
    await databaseRef
        .child('Users')
        .child(userId)
        .child('Map')
        .get()
        .then((dataSnapshot) {
      if (dataSnapshot.exists) {
        Map<String, dynamic> values =
            jsonDecode(jsonEncode(dataSnapshot.value));
       
        final mapData = MapModule.fromJson(values);

        mapModule = mapData;

        print(mapData.longitude);
        print(mapData.longitude);

        return mapModule;
      }
    });
  }
}
