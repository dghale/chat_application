import 'package:chats/auth/screen/splash_screen.dart';
import 'package:chats/chat/store.dart/chat_store.dart';
import 'package:chats/notification/notification.dart';
import 'package:chats/users/store/user_store.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'friends/store/friend_store.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FirebaseDatabase.instance.setPersistenceEnabled(true);
  FirebaseMessaging.onBackgroundMessage(PushNotificationServices.firebaseMessageingBackgroundHandler);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (context) => ChatStore()),
        Provider(create: (context) => UserListStore()),
        Provider(create: (context) => FriendStore())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primaryColor: Colors.blue),
        home: const SplashScreen(),
      ),
    );
  }
}

