import 'package:chats/users/request/user_request.dart';
import 'package:chats/users/screen/contact_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FirebaseGoogleSignIn {
  FirebaseAuth auth = FirebaseAuth.instance;
  User? user;
  UserDatabase userDatabase = UserDatabase();

  //to authenticate user from their google accounts
  Future<User?> signInWithGoogle({required BuildContext context}) async {

    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount? googleSignInAccount =
        await googleSignIn.signIn();

    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleSignInAuthentication.accessToken,
          idToken: googleSignInAuthentication.idToken);

      try {
        User? firebaseUser = (await auth.signInWithCredential(credential)).user;

        user = firebaseUser;

        String username = firebaseUser!.displayName!;
        String email = firebaseUser.email!;
        String userId = firebaseUser.uid;
        String password = ""; // since the google authentication does not require password

        await userDatabase.getAllUser();
        int length = userDatabase.listOfUsers.length;
        //create the user account in database only if the user is signing in for the first time
        if (length > 0) {
          for (var i = 0; i < length; i++) {
            if (userId != userDatabase.listOfUsers[i].userId) {
              await userDatabase.createUserData(
                  email, password, userId, username);
            }
          }
        }

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ContactScreen(
                    email: email,
                    password: password,
                    userId: userId,
                    username: username)));
      } on FirebaseAuthException catch (e) {
        if (e.code == "account-exists-with-different-credential") {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text(
              'The account already exists with a different credential.)',
            ),
          ));
        } else if (e.code == 'invalid-credential') {
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text(
              'Error occurred while accessing credentials. Try again.)',
            ),
          ));
        }
      } catch (e) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(e.toString()),
        ));
      }
    }

    return user;
  }
}