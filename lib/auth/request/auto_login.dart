import 'package:chats/auth/screen/register_screen.dart';
import 'package:chats/users/screen/contact_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:local_auth_android/local_auth_android.dart';
import 'package:local_auth_ios/local_auth_ios.dart';
import 'package:local_auth/error_codes.dart' as auth_error;
import 'package:app_settings/app_settings.dart';

class AutoLogin {
  static Future<bool> localAuth(
      {required BuildContext context,
      required String password,
      required String displayName}) async {
    final LocalAuthentication auth = LocalAuthentication();

    bool isBiometricSupported = await auth.isDeviceSupported();

    final bool canAuthenticateWithBiometrics = await auth.canCheckBiometrics;

    bool isAuthenticated = false;

    User? user = FirebaseAuth.instance.currentUser;

    //gets all the available biometrices in the device
    List<BiometricType> availablebiometrics =
        await auth.getAvailableBiometrics();
    if (availablebiometrics.isEmpty) {
      // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      //   content: const Text("The biometrics is not available in your device."),
      //   action: SnackBarAction(
      //     label: 'OK',
      //     onPressed: () {
      //       AppSettings.openSecuritySettings();
      //     },
      //   ),
      // ));
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text("Warning Message"),
              content: const Flexible(
                  child:
                      Text('The biometrics is not available in your device.')),
              actions: [
                TextButton(
                    onPressed: () {
                      AppSettings.openSecuritySettings();
                    },
                    child: const Text('Set Biometrics')),
                TextButton(
                    onPressed: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const RegisterScreen()),
                          (route) => false);
                    },
                    child: const Text('No, thanks')),
              ],
            );
          });
    } else {
      if (isBiometricSupported && canAuthenticateWithBiometrics) {
        if (availablebiometrics.contains(BiometricType.strong) ||
            availablebiometrics.contains(BiometricType.weak) ||
            availablebiometrics.contains(BiometricType.fingerprint) ||
            availablebiometrics.contains(BiometricType.face)) {
          try {
            isAuthenticated = await auth.authenticate(
                localizedReason: 'Please complete the biometrics to proceed.',
                authMessages: const <AuthMessages>[
                  AndroidAuthMessages(
                    signInTitle: "Biometric authentication required!",
                    cancelButton: 'No thanks',
                  ),
                  IOSAuthMessages(
                    cancelButton: 'No thanks',
                  ),
                ],
                options: const AuthenticationOptions(
                    biometricOnly: true,
                    useErrorDialogs: true,
                    stickyAuth: false,
                    sensitiveTransaction: true));

            isAuthenticated
                ? Navigator.of(context)
                    .pushReplacement(MaterialPageRoute(builder: (context) {
                    return ContactScreen(
                        email: user!.email!,
                        password: password,
                        userId: user.uid,
                        username: displayName);
                  }))
                : Navigator.of(context).pushReplacement(
                    MaterialPageRoute(
                        builder: (context) => const RegisterScreen()),
                  );
          } on PlatformException catch (e) {
            if (e.code == auth_error.notEnrolled) {
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text('The fingerprint is not enrolled.')));
            } else if (e.code == auth_error.lockedOut ||
                e.code == auth_error.permanentlyLockedOut) {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('You have been locked out.')));
            } else if (e.code == auth_error.passcodeNotSet) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: const Text('You should set the passcode'),
                  action: SnackBarAction(
                    onPressed: () {
                     AppSettings.openSecuritySettings();
                    },
                    label: 'OK',
                  )));
            } else if (e.code == auth_error.notEnrolled) {
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text('The fingerprint is not enrolled.')));
            } else if (e.code == auth_error.notAvailable) {
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text('The fingerprint is not available.')));
            }
          }
        }
      }
    }

    return isAuthenticated;
  }

  //here if the user is not null then it will automatically go inside the contacts screen
  static Future<FirebaseApp> initializeFirebase(
      {required BuildContext context,
      required String displayName,
      required String password}) async {
    FirebaseApp firebaseApp = await Firebase.initializeApp();

    User? user = FirebaseAuth.instance.currentUser;

    if (user != null) {
      await localAuth(
          context: context, password: password, displayName: displayName);
    } else {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => const RegisterScreen()),
      );
    }

    return firebaseApp;
  }

  // checkPermission() async{
  //   final fingerPrintPermission = await Permission.pe
  // }
}
