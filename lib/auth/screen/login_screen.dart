import 'package:chats/auth/request/google_sign_in.dart';
import 'package:chats/auth/request/authentication.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  //for form
  final _fkey = GlobalKey<FormState>();

  //to get the texts on email and password field
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  //authentication class instantiated to authenticate the users inside the system
  final _authentication = Authentication();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: const Text('Login'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Form(
          key: _fkey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextFormField(
                  controller: emailController,
                  decoration: const InputDecoration(
                      labelText: 'Email',
                      hintText: 'enter email',
                      border: OutlineInputBorder()),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Enter email address';
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: passwordController,
                  decoration: const InputDecoration(
                      labelText: 'Password',
                      hintText: 'enter password',
                      border: OutlineInputBorder()),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Enter email address';
                    }
                  },
                ),
                ElevatedButton(
                  child: const Text('Go >>'),
                  onPressed: () {
                    if (_fkey.currentState!.validate()) {
                      //users are authenticated inside the system
                      _authentication.authenticateUser(
                          emailController.text.trim(),
                          passwordController.text.trim(),
                          context);
                    }
                  },
                ),
                IconButton(
                    onPressed: () async{
                      //user are authenticated via google
                      await FirebaseGoogleSignIn().signInWithGoogle(context: context);
                    },
                    icon: const Icon(Icons.ac_unit)),
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('Go to Register Screen'))
                
              ],
            ),
          ),
        ),
      ),
    );
  }
}
