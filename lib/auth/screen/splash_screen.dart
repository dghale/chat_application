import 'package:chats/auth/request/auto_login.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    autoLogin();
    super.initState();
  }

  autoLogin() async {
    final pref = await SharedPreferences.getInstance();

    //gets the data of username and password from shared preferences
    final prefUsername = pref.getString('username').toString();
    final prefPassword = pref.getString('password').toString();

    await Future.delayed(const Duration(seconds: 2));

    await AutoLogin.initializeFirebase(
        context: context, displayName: prefUsername, password: prefPassword);
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('Chat'),
      ),
    );
  }
}
