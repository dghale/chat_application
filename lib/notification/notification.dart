import 'package:chats/notification/second_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:firebase_core/firebase_core.dart';

class PushNotificationServices {
  //instantiating
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  RemoteMessage? messageFromNotification;
  static const String _channelId = 'channelid';
  static const String _channelName = 'channel name';
  static const String _channelDescription = 'channel description';

  //initializes the firebase app once the notification is received in the background
  static Future<void> firebaseMessageingBackgroundHandler(
      RemoteMessage message) async {
    ////initializes natively
    await Firebase.initializeApp();
  }

  //instantiating android channel
  static const AndroidNotificationChannel channel = AndroidNotificationChannel(
      _channelId, _channelName,
      description: _channelDescription,
      playSound: true,
      importance: Importance.high);

  //notification details for android platform
  static const AndroidNotificationDetails androidNotificationDetails =
      AndroidNotificationDetails(_channelId, _channelName,
          channelDescription: _channelDescription);

  //when firebase sends notification
  firebaseOnMessage(FlutterLocalNotificationsPlugin localNotificationsPlugin,
      BuildContext context) {
    //notification details
    const platormNotificationDetails =
        NotificationDetails(android: androidNotificationDetails);

    //setting icon for android's notification
    var androidInitializationSettings =
        const AndroidInitializationSettings('@mipmap/ic_launcher');

    const IOSInitializationSettings iosInitializationSettings =
        IOSInitializationSettings(
      requestSoundPermission: false,
      requestBadgePermission: false,
      requestAlertPermission: false,
    );

    var initializeSetting = InitializationSettings(
        android: androidInitializationSettings, iOS: iosInitializationSettings);

    //when notification is selected/clicked
    Future onNotificationSelected(String? payload) async {
      print("payload is $payload");
      await firebaseNavigate(context);
    }

    //initializes the notification plugin
    flutterLocalNotificationsPlugin.initialize(initializeSetting,
        onSelectNotification: onNotificationSelected);

    //shows the notification
    //remotemessage is the data received from firebase
    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) async {});
    //shows when app is open
    FirebaseMessaging.onMessage.listen((RemoteMessage? message) async {
      messageFromNotification = message;

      RemoteNotification notification = message!.notification!;

      print("This is foreground $message");

      await flutterLocalNotificationsPlugin.show(
          0, notification.title, notification.body, platormNotificationDetails);
    });
  }

  static Future<void> _createNotificationChannel(
      FlutterLocalNotificationsPlugin notificationsPlugin) async {
    final androidNotificationChannel = AndroidNotificationChannel(
        channel.id, channel.name,
        description: channel.description, importance: channel.importance);

    await notificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(androidNotificationChannel);
  }

  firebaseNavigate(BuildContext context) {
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage? message) {
      if (message != null) {
        checkNavigate(message, context);
      } else if (messageFromNotification != null) {
        checkNavigate(messageFromNotification, context);
      }
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage? message) async {
      print("onMessageOpenedApp $message");
      checkNavigate(message, context);
    });
  }

  checkNavigate(RemoteMessage? message, BuildContext context) async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SecondPage(message: message.toString())));
  }

  void initilizeNotification(BuildContext context) async {
    FirebaseMessaging.instance.requestPermission();
    //when app is terminated
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage? message) {
      print(message);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage? message) {
      print("This is background $message");
    });
    final FlutterLocalNotificationsPlugin notificationsPlugin =
        FlutterLocalNotificationsPlugin();
    await _createNotificationChannel(notificationsPlugin);
    PushNotificationServices().firebaseOnMessage(notificationsPlugin, context);
  }
}
